import java.io.File
import breeze.linalg._
import scala.collection.mutable.ArrayBuffer
import io.Source;
import breeze.numerics._
import collection.immutable.{Seq => ISeq}

/**
 * Created by polymorpher on 11/7/14.
 */
class Viterbi(transition: DenseMatrix[Double], emission: DenseMatrix[Double], observation: DenseVector[Int]
               ) {
  val initialProbability: DenseVector[Double] = DenseVector.fill(transition.cols)(1.0 / transition.cols)
  val M = transition.cols
  val N = observation.length
  var stateSeq = DenseMatrix.fill(M, N)(-1)

  def solve(): (Double, Seq[Int]) = {
    var Vprev = Array.fill(M)(0.0)
    0 to M - 1 foreach { m =>
      val obs = observation(0)
      Vprev(m) = emission(m, obs) * initialProbability(m)
    }

    1 to N - 1 foreach { n =>
      var Vcurrent = Array.fill(M)(0.0)
      var obs = observation(n)
      0 to M - 1 foreach { m =>
        var maxInd = 0
        var maxVal = transition(0, m) * Vprev(0)
        1 to M - 1 foreach { x =>
          val prob = transition(x, m) * Vprev(x)
          if (prob > maxVal) {
            maxInd = x
            maxVal = prob
          }
        }
        Vcurrent(m) = emission(m, obs) * maxVal
        stateSeq(m, n - 1) = maxInd
      }
      Vprev = Vcurrent
    }
    val maxE = Vprev.zip(0 to Vprev.size).max
    var backtrack = new ArrayBuffer[Int]
    backtrack += maxE._2
    N - 2 to 0 by -1 foreach { n =>
      backtrack += stateSeq(backtrack.last, n)
    }
    //    var prob=initialProbability(backtrack(0))
    //    var state=backtrack(0)
    //    1 to backtrack.size-1 foreach{e=>
    //      prob=prob*transition(state,backtrack(e))
    //      state=backtrack(e)
    //    }
    var prob = 1.0
    backtrack = backtrack.reverse
    0 to backtrack.size - 1 foreach { i =>
      //      println(s"$i ${backtrack(i)} ${observation(i)} ${emission(backtrack(i), observation(i))}")
      prob = prob * emission(backtrack(i), observation(i))
    }
    (prob, backtrack)
  }

  def forwardAlgorithm(): Double = {
    var alpha = DenseVector.fill(M)(-1.0)
    0 to M - 1 foreach { m =>
      alpha(m) = emission(m, observation(0))
    }
    1 to N - 1 foreach { n =>
      println(s"n=$n alpha=$alpha")
      var newAlpha = DenseVector.fill(M)(-1.0)
      val obs = observation(n)
      0 to M - 1 foreach { m =>
        var sum = 0.0
        0 to M - 1 foreach { mm =>
          sum += transition(mm, m) * alpha(mm)
        }
        newAlpha(m) = emission(m, obs) * sum
      }
      alpha = newAlpha
    }
    sum(alpha)
  }
}

class LogViterbi(logTransition: DenseMatrix[Double], logEmission: DenseMatrix[Double], observation: DenseVector[Int]
                  ) {
//  val logInitialProbability: DenseVector[Double] = DenseVector.fill(logTransition.cols)(-Math.log(logTransition.cols))
//  val logInitialProbability: DenseVector[Double] = DenseVector.fill(logTransition.cols)(-Double.MaxValue)
val logInitialProbability: DenseVector[Double] = DenseVector.fill(logTransition.cols)(-10000)
  logInitialProbability(0)= 0
  val N = logTransition.cols
  val T = observation.length
  var stateSeq = DenseMatrix.fill(N, T)(-1)

  def solve(): (Double, Seq[Int]) = {
    var logVprev = Array.fill(N)(0.0)
    0 to N - 1 foreach { m =>
      val obs = observation(0)
      logVprev(m) = logEmission(m, obs) + logInitialProbability(m)
    }

    1 to T - 1 foreach { t =>
      var logVcurrent = Array.fill(N)(0.0)
      var obs = observation(t)
      0 to N - 1 foreach { n =>
        var maxInd = 0
        var maxVal = logTransition(0, n) + logVprev(0)
        1 to N - 1 foreach { x =>
          val prob = logTransition(x, n) + logVprev(x)
          if (prob > maxVal) {
            maxInd = x
            maxVal = prob
          }
        }
        logVcurrent(n) = logEmission(n, obs) + maxVal
        stateSeq(n, t - 1) = maxInd
      }
      logVprev = logVcurrent
    }
//    val maxE = logVprev.zip(0 to logVprev.size).max
    var backtrack = new ArrayBuffer[Int]
//    backtrack += maxE._2
    backtrack += 1
    T - 2 to 0 by -1 foreach { n =>
      backtrack += stateSeq(backtrack.last, n)
    }
    //    var prob=initialProbability(backtrack(0))
    //    var state=backtrack(0)
    //    1 to backtrack.size-1 foreach{e=>
    //      prob=prob*transition(state,backtrack(e))
    //      state=backtrack(e)
    //    }
    var logProb = 0.0
    backtrack = backtrack.reverse
    0 to backtrack.size - 1 foreach { i =>
      //      println(s"$i ${backtrack(i)} ${observation(i)} ${emission(backtrack(i), observation(i))}")
      logProb = logProb + logEmission(backtrack(i), observation(i))
    }
    (logProb, backtrack)
  }
}


class MLELearningHMM(N: Int, K: Int) {
  val transition = DenseMatrix.fill(K, K)(0.2)
  val emission = DenseMatrix.fill(K, N)(0.2)

  def learnExample(observations: Array[Int], statePath: Array[Int]): Unit = {
    val T = observations.length
    var sPrev = 0
    0 to T - 1 foreach { i =>
      val o = observations(i)
      val s = statePath(i)
      emission(s, o) += 1
      if (i != 0) {
        transition(sPrev, s) += 1
      }
      sPrev = s
    }
  }

  def batchLearnExamples(trainingExamples: Seq[(Array[Int], Array[Int])]): Unit = {
    trainingExamples.foreach(e => learnExample(e._1, e._2))
  }

  def learnParameters() = {
    0 to K - 1 foreach { i =>
      val transitionRowSum = sum(transition(i, ::).t)
      0 to K - 1 foreach { j =>
        transition(i, j) /= transitionRowSum
      }
      val emissionRowSum = sum(emission(i, ::).t)
      0 to N - 1 foreach { j =>
        emission(i, j) /= emissionRowSum
      }
    }

  }
}



object HMM {
  def writeTestResult(transition: DenseMatrix[Double], emission: DenseMatrix[Double],
                      reader: NERTaskReader, outputFilename: String) = {
    val p = new java.io.PrintWriter(new File(outputFilename))
    var numSentenceProcessed = 0
    val logTransition=log(transition)
    val logEmission =log(emission)
    reader.testSentences.foreach { sentence =>
      val viterbiSolver = new LogViterbi(logTransition  , logEmission , DenseVector(sentence.words.toArray))
      //      val viterbiSolver = new Viterbi(transition, emission, DenseVector(sentence.words.toArray))
      val (prob, hiddenStates) = viterbiSolver.solve
      val translatedSeq = hiddenStates.map(e=>reader.tagDic(e))
      println(s"Processed sentence $numSentenceProcessed prob=$prob seq=$hiddenStates")
      println(translatedSeq)
      p.println(sentence.toString(reader, hiddenStates))
      p.flush()
      numSentenceProcessed += 1
    }
    p.close()
  }

  def main(args: Array[String]) {
    if (args.size < 3) {
      println("Arguments: <trainingFile> <testFile> <outputFile>")
      System.exit(-1)
    }
    val reader = new NERTaskReader(args(0), args(1))
    reader.printStatus
    val mleLearner = new MLELearningHMM(reader.wordDic.size, reader.tagDic.size)
    var numExamples = 0
    0 to reader.trainingSentences.size - 1 foreach { i =>
      if (numExamples % 100 == 0) println(s"Processed $numExamples examples")
      mleLearner.learnExample(reader.trainingSentences(i).words.toArray, reader.trainingSentences(i).tags.toArray)
      numExamples += 1
    }
    //    reader.trainingSentences.foreach{e =>
    //      if(numExamples%100==0)println(s"Processed $numExamples examples")
    //      mleLearner.learnExample(e.words.toArray, e.tags.toArray)
    //      numExamples+=1
    //    }
    mleLearner.learnParameters()
    val trainedTransition = mleLearner.transition
    val trainedEmission = mleLearner.emission

    //    val testReader = new NERTaskReader("/test")
    //    println(sum(mleLearner.transition(*, ::)))
    //    println(sum(mleLearner.emission(*, ::)))
    //    val devReader = new

    writeTestResult(trainedTransition, trainedEmission, reader, args(2))
  }
}
