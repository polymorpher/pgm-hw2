import java.io.File
import breeze.linalg._
import scala.collection.mutable.ArrayBuffer
import io.Source;
import breeze.numerics._
import collection.immutable.{Seq => ISeq}

/**
 * Created by polymorpher on 11/7/14.
 */

class LogViterbiCRF(sentence: SentenceWithFeatures, weights: collection.mutable.Map[Seq[Int], Double], N: Int) {

  //  val logInitialProbability: DenseVector[Double] = DenseVector.fill(N)(-Math.log(N))
  val logInitialProbability: DenseVector[Double] = DenseVector.fill(N)(-10000)
  logInitialProbability(0) = 0
  val T = sentence.words.length
  var stateSeq = DenseMatrix.fill(N, T)(-1)

  def sumWeights(features: Seq[Seq[Int]]): Double = {
    val ret = features.foldLeft(0.0)((r, e) => r + weights.get(e).getOrElse(0.0))
    //    println(ret)
    ret
  }

  def solve(): Seq[Int] = {
    var logVprev = Array.fill(N)(0.0)
    0 to N - 1 foreach { m =>
      logVprev(m) = logInitialProbability(m)
    }

    1 to T - 1 foreach { t =>
      var logVcurrent = Array.fill(N)(0.0)

      0 to N - 1 foreach { n =>
        val enumFeatures = enumerateFeatures(t, n, 0)
        var maxInd = 0
        var maxVal = sumWeights(enumFeatures)
        1 to N - 1 foreach { x =>
          val features = enumerateFeatures(t, n, x)
          val prob = sumWeights(features) + logVprev(x)
          if (prob > maxVal) {
            maxInd = x
            maxVal = prob
          }
        }
        logVcurrent(n) = maxVal
        stateSeq(n, t - 1) = maxInd
      }
      logVprev = logVcurrent
    }
    //    val maxE = logVprev.zip(0 to logVprev.size).max
    var backtrack = new ArrayBuffer[Int]
    //    backtrack += maxE._2
    backtrack += 1
    T - 2 to 0 by -1 foreach { n =>
      backtrack += stateSeq(backtrack.last, n)
    }
    //    var prob=initialProbability(backtrack(0))
    //    var state=backtrack(0)
    //    1 to backtrack.size-1 foreach{e=>
    //      prob=prob*transition(state,backtrack(e))
    //      state=backtrack(e)
    //    }
    var logProb = 0.0
    backtrack = backtrack.reverse
    //    0 to backtrack.size - 1 foreach { i =>
    //      //      println(s"$i ${backtrack(i)} ${observation(i)} ${emission(backtrack(i), observation(i))}")
    //      logProb = logProb +
    //    }
    //    (logProb, backtrack)
    backtrack
  }

  def enumerateFeatures(t: Int, x: Int, xp: Int): Seq[Seq[Int]] = {
    val s = sentence
    val word = s.words(t)
    val state = x
    val statePrev = xp
    val lword = s.lwords(t)
    val pos = s.posTags(t)
    val shape = s.shapes(t)
    val gaz = s.gaz(t)
    val pre = s.pre(t)
    val cap = s.cap(t)
    val position = t

    val buf = new ArrayBuffer[Seq[Int]]()
    pre.foreach { p =>
      buf += Seq(12, p, 16, state)
    }
    buf ++= Seq(
      Seq(0, word, 16, state),
      Seq(0, word, 16, state, 17, statePrev),
      Seq(1, lword, 16, state),
      Seq(1, lword, 16, state, 17, statePrev),
      Seq(2, pos, 16, state),
      Seq(2, pos, 16, state, 17, statePrev),
      Seq(3, shape, 16, state),
      Seq(3, shape, 16, state, 17, statePrev),
      Seq(13, gaz, 16, state),
      Seq(14, cap, 16, state),
      Seq(15, position, 16, state),
      Seq(0, word, 1, lword, 16, state),
      Seq(0, word, 2, pos, 16, state),
      Seq(0, word, 3, shape, 16, state),
      Seq(1, lword, 2, pos, 16, state),
      Seq(1, lword, 3, shape, 16, state),
      Seq(2, pos, 3, shape, 16, state)
    )

    if (t != 0) {
      val wordPrev = s.words(t - 1)
      val lwordPrev = s.lwords(t - 1)
      val posPrev = s.posTags(t - 1)
      val shapePrev = s.shapes(t - 1)
      buf ++= Seq(
        Seq(4, wordPrev, 16, state),
        Seq(4, wordPrev, 16, state, 17, statePrev),
        Seq(5, lwordPrev, 16, state),
        Seq(5, lwordPrev, 16, state, 17, statePrev),
        Seq(6, posPrev, 16, state),
        Seq(6, posPrev, 16, state, 17, statePrev),
        Seq(7, shapePrev, 16, state),
        Seq(7, shapePrev, 16, state, 17, statePrev),
        Seq(0, word, 4, wordPrev, 16, state),
        Seq(0, word, 5, lwordPrev, 16, state),
        Seq(0, word, 6, posPrev, 16, state),
        Seq(0, word, 7, shapePrev, 16, state),
        Seq(1, lword, 4, wordPrev, 16, state),
        Seq(1, lword, 5, lwordPrev, 16, state),
        Seq(1, lword, 6, posPrev, 16, state),
        Seq(1, lword, 7, shapePrev, 16, state),
        Seq(2, pos, 4, wordPrev, 16, state),
        Seq(2, pos, 5, lwordPrev, 16, state),
        Seq(2, pos, 6, posPrev, 16, state),
        Seq(2, pos, 7, shapePrev, 16, state),
        Seq(3, shape, 4, wordPrev, 16, state),
        Seq(3, shape, 5, lwordPrev, 16, state),
        Seq(3, shape, 6, posPrev, 16, state),
        Seq(3, shape, 7, shapePrev, 16, state)
      )
    }
    if (t != T - 1) {
      val wordNext = s.words(t + 1)
      val lwordNext = s.lwords(t + 1)
      val posNext = s.posTags(t + 1)
      val shapeNext = s.shapes(t + 1)
      buf ++= Seq(
        Seq(8, wordNext, 16, state),
        Seq(8, wordNext, 16, state, 17, statePrev),
        Seq(9, lwordNext, 16, state),
        Seq(9, lwordNext, 16, state, 17, statePrev),
        Seq(10, posNext, 16, state),
        Seq(10, posNext, 16, state, 17, statePrev),
        Seq(11, shapeNext, 16, state),
        Seq(11, shapeNext, 16, state, 17, statePrev),
        Seq(0, word, 8, wordNext, 16, state),
        Seq(0, word, 9, lwordNext, 16, state),
        Seq(0, word, 10, posNext, 16, state),
        Seq(0, word, 11, shapeNext, 16, state),
        Seq(1, lword, 8, wordNext, 16, state),
        Seq(1, lword, 9, lwordNext, 16, state),
        Seq(1, lword, 10, posNext, 16, state),
        Seq(1, lword, 11, shapeNext, 16, state),
        Seq(2, pos, 8, wordNext, 16, state),
        Seq(2, pos, 9, lwordNext, 16, state),
        Seq(2, pos, 10, posNext, 16, state),
        Seq(2, pos, 11, shapeNext, 16, state),
        Seq(3, shape, 8, wordNext, 16, state),
        Seq(3, shape, 9, lwordNext, 16, state),
        Seq(3, shape, 10, posNext, 16, state),
        Seq(3, shape, 11, shapeNext, 16, state)
      )
    }
    buf.toSeq
  }

}

object FeatureMap {
  val featureMap = Map(
    "Wi" -> 0,
    "Oi" -> 1,
    "Pi" -> 2,
    "Si" -> 3,
    "Wi-1" -> 4,
    "Oi-1" -> 5,
    "Pi-1" -> 6,
    "Si-1" -> 7,
    "Wi+1" -> 8,
    "Oi+1" -> 9,
    "Pi+1" -> 10,
    "Si+1" -> 11,
    "PREi" -> 12,
    "GAZi" -> 13,
    "CAPi" -> 14,
    "POSi" -> 15,
    "Ti" -> 16,
    "Ti-1" -> 17
  )
  val rFeatureMap = collection.mutable.Map[Int,String]()
  featureMap.foreach{kv=>
    rFeatureMap += ((kv._2 -> kv._1))
  }
  def apply(s: String) = featureMap(s)
  def apply(i:Int) = rFeatureMap(i)
}

class SentenceWithFeatures(words: Seq[Int], posTags: Seq[Int], syntaticTags: Seq[Int], tags: Seq[Int],
                           val shapes: Seq[Int], val lwords: Seq[Int], val gaz: Seq[Int], val cap: Seq[Int],
                           val pre: Seq[Seq[Int]])
  extends Sentence(words, posTags, syntaticTags, tags) {
//shapes, lwords, gaz, cap, pre)
}

class NERTaskReaderWithFeatures(trainingFileName: String, testFileName: String,
                                gazetteerFileName: String) extends NERTaskReader(trainingFileName, testFileName) {

  def isDelim(s: String): Boolean = (s == "<START>" || s == "<STOP>")

  def isDelim(i: Int): Boolean = isDelim(wordDic(i))

  val lwordDic = new Dictionary
  wordDic.dic.foreach { kv => if (!isDelim(kv._1)) lwordDic.add(kv._1.toLowerCase)}
  val shapeDic = new Dictionary
  val gazetteer = new Dictionary
  val prefixDic = new Dictionary

  Source.fromFile(gazetteerFileName).getLines().foreach { line =>
    val gaz = line.substring(line.indexOf(" ") + 1)
    assert(!gaz.isEmpty)
    gazetteer add gaz
//    gaz.split(" ").foreach{g=>
//      gazetteer add g
//    }

  }
  println(gazetteer.dic)

  private[this] def toShape(s: String) = {
    val a = new Array[Char](s.length)
    0 to s.length - 1 foreach { i =>
      val c = s(i)
      if (c >= 'a' && c <= 'z') a(i) = 'a'
      else if (c >= 'A' && c <= 'Z') a(i) = 'A'
      else if (c >= '0' && c <= '9') a(i) = 'd'
      else a(i) = c
    }
    a.mkString

  }

  wordDic.dic.foreach { kv => if (!isDelim(kv._1)) shapeDic.add(toShape(kv._1))}
  0 to trainingSentences.length - 1 foreach { i =>
    val sent = trainingSentences(i)
    val shapes = sent.words.map { e => if (isDelim(e)) e else shapeDic(toShape(wordDic(e)))}
    val lwords = sent.words.map(e => if (isDelim(e)) e else lwordDic(wordDic(e).toLowerCase))
    val gaz = sent.words.map(e => if (gazetteer(wordDic(e)) == -1) 0 else 1)
    val cap = sent.words.map { e =>
      val w = wordDic(e)
      if (w(0) >= 'A' && w(0) <= 'Z') 1 else 0
    }
    val pre = sent.words.map { e =>
      val w = wordDic(e)
      if (isDelim(e))
        Seq[Int]()
      else {
        val prefixes = 1 to Math.min(w.length, 4) map { prefixLength =>
          prefixDic.add(w.substring(0, prefixLength))
        }
        prefixes.toSeq
      }
    }.toSeq

    trainingSentences(i) = new SentenceWithFeatures(sent.words, sent.posTags, sent.syntaticTags, sent.tags,
      shapes, lwords, gaz, cap, pre)
  }
  0 to testSentences.length - 1 foreach { i =>
    val sent = testSentences(i)
    val shapes = sent.words.map { e => if (isDelim(e)) e else shapeDic(toShape(wordDic(e)))}
    val lwords = sent.words.map(e => if (isDelim(e)) e else lwordDic(wordDic(e).toLowerCase))
    val gaz = sent.words.map(e => if (gazetteer(wordDic(e)) == -1) 0 else 1)
    val cap = sent.words.map { e =>
      val w = wordDic(e)
      if (w(0) >= 'A' && w(0) <= 'Z') 1 else 0
    }
    val pre = sent.words.map { e =>
      val w = wordDic(e)
      if (isDelim(e))
        Seq[Int]()
      else {
        val prefixes = 1 to Math.min(w.length, 4) map { prefixLength =>
          prefixDic.add(w.substring(0, prefixLength))
        }
        prefixes.toSeq
      }
    }.toSeq
    testSentences(i) = new SentenceWithFeatures(sent.words, sent.posTags, sent.syntaticTags, sent.tags,
      shapes, lwords, gaz, cap, pre)

  }

  val featureWeight = collection.mutable.Map[Seq[Int], Double]()

  def translateFeatureSeq(s:Seq[Int]):String={
    val sb=new StringBuilder
    0 to s.length-1 by 2 foreach{i=>
      val fname=FeatureMap(s(i))
      val fid=s(i)
      val fval=s(i+1)
      var fvalStr=""
      if (fid == 0 || fid == 4 || fid == 8) fvalStr = wordDic(fval)
      if (fid == 1 || fid == 5 || fid == 9) fvalStr = lwordDic(fval)
      if (fid == 2 || fid == 6 || fid == 10) fvalStr = posDic(fval)
      if (fid == 3 || fid == 7 || fid == 11) fvalStr = shapeDic(fval)
      if (fid == 12) fvalStr = prefixDic(fval)
      if (fid == 13) fvalStr = fval.toString
      if (fid == 14) fvalStr = fval.toString
      if (fid == 15) fvalStr = fval.toString
      if (fid == 16 || fid == 17) fvalStr = tagDic(fval)
      if(i!=0){sb.append(":")}
      sb.append(s"$fname=$fvalStr")
    }
    sb.toString
  }
  def readWeightFile(weightFile: String): Unit = {
    var numWeightsProcessed = 0
    Source.fromFile(weightFile).getLines.foreach { line =>
      if (numWeightsProcessed % 10000 == 0)
        println(s"numWeightsProcessed=$numWeightsProcessed")
      val parts = line.split(" ")
      //      println(parts.mkString(" "))
      val kvsUnprocessed = parts(0).split(":")
      val kvsBuffer = new ArrayBuffer[String]
      kvsUnprocessed.foreach { kvStr =>
        if (kvStr.contains("="))
          kvsBuffer += kvStr
        else {
          val str = kvsBuffer(kvsBuffer.length - 1)
          kvsBuffer(kvsBuffer.length - 1) = str +":"+ kvStr
        }
      }
      val kvs = kvsBuffer.toArray
      var weight = parts(1).toDouble
      var skipFeatureSet = false
      var featureSeqBuffer = new ArrayBuffer[(Int, Int)]()
      kvs.foreach { kvStr =>
        val kvparts = kvStr.split("=")
        val featureName = kvparts(0)
        val featureVal = kvStr.substring(featureName.length + 1)
        //        println(featureName+" "+featureVal)
        val fid = FeatureMap(featureName)
        var fval = 0
        if (fid == 0 || fid == 4 || fid == 8) fval = wordDic.add(featureVal)
        if (fid == 1 || fid == 5 || fid == 9) fval = lwordDic.add(featureVal)
        if (fid == 2 || fid == 6 || fid == 10) fval = posDic.add(featureVal)
        if (fid == 3 || fid == 7 || fid == 11) fval = shapeDic.add(featureVal)
        if (fid == 12) fval = prefixDic.add(featureVal)
        if (fid == 13) fval = if (featureVal.toBoolean) 1 else 0
        if (fid == 14) fval = if (featureVal.toBoolean) 1 else 0
        if (fid == 15) fval = featureVal.toInt
        if (fid == 16 || fid == 17) fval = tagDic.add(featureVal)
        assert(fval >= 0)
        featureSeqBuffer += ((fid, fval))
      }
      val flatFeatureSeqBuffer = new ArrayBuffer[Int]()
      featureSeqBuffer.sorted.foreach { e =>
        flatFeatureSeqBuffer += e._1
        flatFeatureSeqBuffer += e._2
      }
      2 to flatFeatureSeqBuffer.length - 1 foreach { i =>
        if (i % 2 == 0) assert(flatFeatureSeqBuffer(i) > flatFeatureSeqBuffer(i - 2))
      }
      if (!skipFeatureSet) {
        if (featureWeight.get(flatFeatureSeqBuffer.toSeq).isDefined) {
          val oldWeight = featureWeight.get(flatFeatureSeqBuffer.toSeq).get
          println(s"Feature ${flatFeatureSeqBuffer.toSeq} is reappearing, oldWeight=$oldWeight newWeight=$weight")
          println(translateFeatureSeq(flatFeatureSeqBuffer))
          weight = weight + oldWeight
        }
        featureWeight += (flatFeatureSeqBuffer.toSeq -> weight)
      }
      numWeightsProcessed += 1
    }
  }

  override def printStatus(): Unit = {
    super.printStatus
    println(s"numWeightRules=${featureWeight.size}")
    println(s"numGazetteer=${gazetteer.size}")
    println(s"numShapes=${shapeDic.size}")
    println(s"numPrefixes=${prefixDic.size}")
    println(s"numLowerCaseWords=${lwordDic.size}")

  }

}

object CRF {
  def writeTestResult(reader: NERTaskReaderWithFeatures, outputFilename: String) = {

    val p = new java.io.PrintWriter(new File(outputFilename))
    var numSentenceProcessed = 0
    reader.testSentences.foreach { sentence =>
      val crfViterbiSolver = new LogViterbiCRF(sentence.asInstanceOf[SentenceWithFeatures],
        reader.featureWeight, reader.tagDic.size())
      //      val viterbiSolver = new Viterbi(transition, emission, DenseVector(sentence.words.toArray))
      val hiddenStates = crfViterbiSolver.solve.toBuffer
      //      hiddenStates(hiddenStates.length-1)=reader.tagDic("<STOP>")
      val translatedSeq = hiddenStates.map(e => reader.tagDic(e))
      println(s"Processed sentence $numSentenceProcessed, Seq:$hiddenStates")
      println(translatedSeq)
      p.println(sentence.toString(reader, hiddenStates))
      p.flush()
      numSentenceProcessed += 1
    }
    p.close()
  }

  def main(args: Array[String]) {
    if (args.size < 5) {
      println("Arguments: <trainingFile> <testFile> <weightFile> <gazetteerFile> <outputFile>")
      System.exit(-1)
    }
    val reader = new NERTaskReaderWithFeatures(args(0), args(1), args(3))

    reader.readWeightFile(args(2))
    reader.printStatus
    writeTestResult(reader, args(4))
  }
}
