import scala.collection.mutable.ArrayBuffer
import scala.io.Source

class Dictionary {
  val dic = collection.mutable.Map[String, Int]()
  val rdic = collection.mutable.Map[Int, String]()

  def add(s: String): Int = {
    val id = dic.get(s)
    if (id.isDefined) {
      return id.get
    } else {
      val nid = dic.size
      dic.put(s, nid)
      rdic.put(nid, s)
      nid
    }
  }

  def size(): Int = dic.size

  def apply(i: Int) = rlookup(i)

  def apply(s: String) = lookup(s)

  def lookup(s: String): Int = dic.get(s).getOrElse(-1)

  def rlookup(i: Int): String = rdic(i)

  add("<START>")
  add("<STOP>")
}

object Dictionary {
  def translate(tokens: Seq[Int], localDic: Dictionary, remoteDic: Dictionary): Seq[Int] =
    tokens.map(e => localDic(remoteDic(e)))

  def countOOVTypes(localDic: Dictionary, remoteDic: Dictionary): Int = {
    localDic.dic.keys.foldLeft(0)((count, s) => if (remoteDic.dic.get(s).isDefined) count + 1 else count)
  }
}

class Sentence(val words: Seq[Int], val posTags: Seq[Int], val syntaticTags: Seq[Int], val tags: Seq[Int]) {
  def toString(reader: NERTaskReader, newTags: Seq[Int]): String = {
    val s = new StringBuilder
    0 to words.size - 1 foreach { i =>
      if (words(i) != 0 && words(i) != 1) {
        s append s"${reader.wordDic(words(i))} ${reader.posDic(posTags(i))}" +
          s" ${reader.syntaticDic(syntaticTags(i))} ${reader.tagDic(tags(i))}" +
          s" ${reader.tagDic(newTags(i))}" + "\n"
      }
    }
    s.toString()
  }

  def size(): Int = {
    words.size
  }
}

class NERTaskReader(trainingFileName: String, testFileName: String) {
  val wordDic = new Dictionary()
  val tagDic = new Dictionary()
  val posDic = new Dictionary()
  protected[this] val wordsBuffer = new ArrayBuffer[Int]
  protected[this] val posTagsBuffer = new ArrayBuffer[Int]
  protected[this] val syntaticTagsBuffer = new ArrayBuffer[Int]
  protected[this] val tagsBuffer = new ArrayBuffer[Int]

  protected[this] def clearBuffer = {
    wordsBuffer.clear;
    posTagsBuffer.clear;
    syntaticTagsBuffer.clear;
    tagsBuffer.clear

  }

  protected[this] def toImSeq[T](sq: collection.mutable.Seq[T]): collection.immutable.Seq[T] =
    collection.immutable.Seq[T](sq: _*)

  val syntaticDic = new Dictionary()
  val trainingSentences = new ArrayBuffer[Sentence]()
  val testSentences = new ArrayBuffer[Sentence]()
  var numTrainingTokens = 0
  var numTestTokens = 0
  var oovCount = 0
  //Constructor

  Source.fromFile(trainingFileName).getLines().foreach { line =>
    val parts = line.split(" ")
    if (parts.size < 3) {
      if (wordsBuffer.size > 0) {
        wordsBuffer += wordDic("<STOP>");
        posTagsBuffer += posDic("<STOP>");
        syntaticTagsBuffer += syntaticDic("<STOP>");
        tagsBuffer += tagDic("<STOP>")
        trainingSentences += new Sentence(toImSeq(wordsBuffer), toImSeq(posTagsBuffer),
          toImSeq(syntaticTagsBuffer), toImSeq(tagsBuffer))
        clearBuffer
        wordsBuffer += wordDic("<START>");
        posTagsBuffer += posDic("<START>");
        syntaticTagsBuffer += syntaticDic("<START>");
        tagsBuffer += tagDic("<START>")
        numTrainingTokens += 2
      }
    } else {
      wordsBuffer += wordDic.add(parts(0))
      posTagsBuffer += posDic.add(parts(1))
      syntaticTagsBuffer += syntaticDic.add(parts(2))
      tagsBuffer += tagDic.add(parts(3))
      numTrainingTokens += 1
    }
  }
  if (wordsBuffer.size > 0) {
    wordsBuffer += wordDic("<STOP>");
    posTagsBuffer += posDic("<STOP>");
    syntaticTagsBuffer += syntaticDic("<STOP>");
    tagsBuffer += tagDic("<STOP>")
    trainingSentences += new Sentence(toImSeq(wordsBuffer), toImSeq(posTagsBuffer),
      toImSeq(syntaticTagsBuffer), toImSeq(tagsBuffer))
    clearBuffer
    wordsBuffer += wordDic("<START>");
    posTagsBuffer += posDic("<START>");
    syntaticTagsBuffer += syntaticDic("<START>");
    tagsBuffer += tagDic("<START>")
    numTrainingTokens += 2
  }

  Source.fromFile(testFileName).getLines().foreach { line =>
    val parts = line.split(" ")
    if (parts.size < 3) {
      if (wordsBuffer.size > 0) {
        wordsBuffer += wordDic("<STOP>");
        posTagsBuffer += posDic("<STOP>");
        syntaticTagsBuffer += syntaticDic("<STOP>");
        tagsBuffer += tagDic("<STOP>")
        testSentences += new Sentence(toImSeq(wordsBuffer), toImSeq(posTagsBuffer),
          toImSeq(syntaticTagsBuffer), toImSeq(tagsBuffer))
        clearBuffer
        wordsBuffer += wordDic("<START>");
        posTagsBuffer += posDic("<START>");
        syntaticTagsBuffer += syntaticDic("<START>");
        tagsBuffer += tagDic("<START>")
        numTestTokens += 2
      }
    } else {
      if (wordDic.dic.get(parts(0)).isEmpty) oovCount += 1
      wordsBuffer += wordDic.add(parts(0))
      posTagsBuffer += posDic.add(parts(1))
      syntaticTagsBuffer += syntaticDic.add(parts(2))
      tagsBuffer += tagDic.add(parts(3))
      numTestTokens += 1
    }
  }

  if (wordsBuffer.size > 0) {
    wordsBuffer += wordDic("<STOP>");
    posTagsBuffer += posDic("<STOP>");
    syntaticTagsBuffer += syntaticDic("<STOP>");
    tagsBuffer += tagDic("<STOP>")
    testSentences += new Sentence(toImSeq(wordsBuffer), toImSeq(posTagsBuffer),
      toImSeq(syntaticTagsBuffer), toImSeq(tagsBuffer))
    clearBuffer
    wordsBuffer += wordDic("<START>");
    posTagsBuffer += posDic("<START>");
    syntaticTagsBuffer += syntaticDic("<START>");
    tagsBuffer += tagDic("<START>")
    numTestTokens += 2
  }


  val trainingSentenceAverageLength = trainingSentences.foldLeft(0)((r, e) => r + e.words.size) / trainingSentences.size.toDouble
  val testSentenceAverageLength = testSentences.foldLeft(0)((r, e) => r + e.words.size) / testSentences.size.toDouble

  def printStatus(): Unit = {
    println(s"numTrainingTokens=$numTrainingTokens numTrainingSentences=${trainingSentences.size} trainingSentenceAverageLength=$trainingSentenceAverageLength ")
    println(s"numTestTokens=$numTestTokens numTestSentences=${testSentences.size} testSentenceAverageLength=$testSentenceAverageLength")
    println(s"oovRate= ${oovCount.toDouble / wordDic.size.toDouble}")
    println(s"numWordTypes=${wordDic.size} numPosTypes=${posDic.size} numSyntaticTagTypes=${syntaticDic.size} numTagsSize=${tagDic.size}")

  }
}